package cf.mindaugas.sdademoklp._4_5_staticInnerClass;

public class Main {
    public static void main(String[] args) {
        Bicycle bicycle = new Bicycle();
        System.out.println(bicycle.getMaxSpeed()); // Prints 25

        Bicycle.Mechanic mechanic = new Bicycle.Mechanic();
        mechanic.repair(bicycle);
        System.out.println(bicycle.getMaxSpeed()); // Prints 40
    }
}

class Bicycle {
    private int maxSpeed = 25;
    public int getMaxSpeed() {
        return maxSpeed;
    }
    public static class Mechanic {
        public void repair(Bicycle bicycle) {
            // nested static class can refer
            // private field of outer class
            bicycle.maxSpeed += 15;
        }
    }
}
