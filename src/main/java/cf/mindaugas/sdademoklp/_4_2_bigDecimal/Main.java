package cf.mindaugas.sdademoklp._4_2_bigDecimal;

import java.math.BigDecimal;

public class Main {
    public static void main(String[] args) {

        // https://stackoverflow.com/questions/3413448/double-vs-bigdecimal
        double a = 0.02;
        double b = 0.03;
        double c = b - a;
        System.out.println(c);

        // BigDecimal _a = new BigDecimal(5);
        BigDecimal _a = new BigDecimal("0.02");
        BigDecimal _b = new BigDecimal("0.03");
        BigDecimal _c = _b.subtract(_a);
        System.out.println(_c);
    }
}
