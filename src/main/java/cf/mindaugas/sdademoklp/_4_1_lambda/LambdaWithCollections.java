package cf.mindaugas.sdademoklp._4_1_lambda;

import java.util.ArrayList;
import java.util.List;

public class LambdaWithCollections {
    public static void main(String[] args) {

        List<Person> people = new ArrayList<>() {{
            add(new Person(63, "Mindaugas"));
            add(new Person(21, "Pranas"));
            add(new Person(33, "Petras"));
        }};


        // Ex0: .forEach w/ method reference operator
        // p --> temporary variable, that forEach generates
        // people.forEach(p -> System.out.println(p));
        // people.forEach(System.out::println);
        people.forEach(p -> System.out.println(p.getAge()));

        // ... equivalent to
        for (Person p : people)
            System.out.println(p);

        // Ex2: .removeIf w/ a predicate function (a predicate function
        // .. is a special lambda expression that returns true or false)
        people.removeIf(person -> person.getAge() > 50); // this could be a predicate
        System.out.println("After removing people > 50: ");
        people.forEach(System.out::println);
    }
}

class Person implements Comparable<Person>{
    private int age;
    private String name;

    public Person(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (age != person.age) return false;
        return name != null ? name.equals(person.name) : person.name == null;
    }

    @Override
    public int hashCode() {
        int result = age;
        result = 31 * result + name.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public int compareTo(Person o) {
        return -1 * this.age - o.age;
    }
}
