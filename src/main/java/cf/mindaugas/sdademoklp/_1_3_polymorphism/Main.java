package cf.mindaugas.sdademoklp._1_3_polymorphism;

public class Main {
    public static void main(String[] args) {
        // String[] strings = {"A", "B", "C"};

        Employee[] employees = {
                new Employee("Employee1"),
                new Employee("Employee2"),
                // new Humanoid() // this will not work, because we declared Employee[] array
                new Nurse("Jonas", "XXL") // this will work because arrays are polymorphic in Java
        };

        for(int i = 0; i < employees.length; i++) {
            System.out.println("Length of employee: " + employees[i].getName() + " is: " + getNameLenght(employees[i]));
        }
    }

    public static int getNameLenght(Employee e){
        return e.getName().length();
    }
}

class Humanoid { }

class Employee {
    private String name;
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Employee(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                '}';
    }
}

class Nurse extends Employee {
    private String foodTrolley;
    public Nurse(String name, String foodTrolley) {
        super(name);
        this.foodTrolley = foodTrolley;
    }

    @Override
    public String toString() {
        return "Nurse{" + "foodTrolley='" + foodTrolley + '\'' +"} " + super.toString();
    }
}

class Doctor extends Employee {
    public Doctor(String name) {
        super(name);
    }
}

