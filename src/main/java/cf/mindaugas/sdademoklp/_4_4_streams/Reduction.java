package cf.mindaugas.sdademoklp._4_4_streams;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class Reduction {
    public static void main(String[] args) {
        // // 0. In built reductions
        // int[] vals = { 2, 4, 6, 8, 10, 12, 14, 16 };
        // int sum = Arrays.stream(vals).sum();
        // System.out.printf("The sum of values: %d%n", sum);

        // 1. More explicitly, without using in-built reductions
        // Creating list of integers
        List<Integer> array = Arrays.asList(2, 4, 6, 8, 10, 12, 14, 16);
        // Finding sum of all elements
        // - initial value --> 0
        // - partialResult --> represents the result of all the previous operations
        // - currentElement --> just the next element from the stream
        int sum = array.stream().reduce(0, (partialResult, currentElement) -> partialResult + currentElement);
        System.out.printf("The sum of values: %d%n", sum);

        List<Car> cars = Arrays.asList(
                new Car("Skoda", 18544),
                new Car("Volvo", 27344),
                new Car("Fiat", 23650),
                new Car("Renault", 19700)
        );

        Optional<Car> car = cars.stream()
                .reduce((c1, c2) -> c1.getPrice() > c2.getPrice() ? c1 : c2);
        car.ifPresent(System.out::println);


        // 2. More complex example - string concatenation
        // Initial value is empty string.
        // If current result value is an empty string we simply add element to an empty string,
        // otherwise we concatenate current value, ", " and element.
        List<String> names = Arrays.asList("Andrew", "Bu", "Michael");
        String namesConcatenation = names.stream()
                // .reduce("", (currValue, element) -> currValue + ", " + element);
                .reduce("", (currValue, element) -> (currValue.equals("") ? "" : currValue + ", ") + element);
        // 0. currValue -> "" ... returned "" + "Andrew"
        // 1. currValue -> "" + "Andrew" ... returned "" + "Andrew" + ", " + "Bu"
        // 2. currValue -> "" + "Andrew" + ", " + "Bu" ... returned "" + "Andrew" + ", " + "Bu" + ", " + "Michael"
        System.out.println(namesConcatenation);


        List<String> names2 = Arrays.asList("Andrew", "Bu", "Michael");
        String s = "";
        for(int i = 0; i < names2.size() - 1; i++)
            s += names2.get(i) + ", ";
        s += names2.get(names2.size() - 1);
        System.out.println(s);

        // Sorting with streams
        List<Person> people = Arrays.asList(
                new Person("John", 40),
                new Person("Sarah", 30),
                new Person("John", 35)
        );

        people.stream()
                .sorted((p1, p2) -> p1.name.compareTo(p2.name))
                .forEach(p -> System.out.println(p.name));

        // alternative way:
        people.stream()
                .sorted(Comparator.comparing(p -> p.name))
                .forEach(p -> System.out.println(p.name));

        // sorting by two fields:
        System.out.println("==========================");
        people.stream()
                .sorted(Comparator.comparing((Person p) -> p.name)
                        .thenComparing((Person p) -> p.age))
                .forEach(System.out::println);
    }
}

class Car {

    private final String name;
    private final int price;

    public Car(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public int getPrice() {
        return price;
    }



    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Car{name=").append(name).append(", price=")
                .append(price).append("}");

        return builder.toString();
    }
}
