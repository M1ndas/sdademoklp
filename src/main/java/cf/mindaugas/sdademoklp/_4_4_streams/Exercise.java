package cf.mindaugas.sdademoklp._4_4_streams;

import java.util.Arrays;
import java.util.List;

public class Exercise {
    public static void main(String[] args) {
        // Let's assume we have a list of people.
        List<Person> people = Arrays.asList(
                new Person("Thomas", 55),
                new Person("Jonas", 22),
                new Person("Petras", 33),
                new Person("Thomas", 22)
        );

        // We would like to get the average age of people named "Thomas" w/ streams.
        // - 1. Create stream
        // - 2. Filter Thomas
        // - 3. Get their avg. age (will need to convert to int to get a collection of ints)
        people.stream()
                .filter(person -> person.name.equals("Thomas"))
                .mapToInt(person -> person.age)
                .average()
                .ifPresent(System.out::println);


        // allMatch and anyMatch

        List<String> names = Arrays.asList("Andrew", "Bu", "Michael");
        boolean allNamesLengthIsGtThan3 = names.stream().allMatch(n -> n.length() > 3);
        boolean thereIsANameWhichLengthIsGtThan3 = names.stream().anyMatch(n -> n.length() > 3);


        // // functional style
        // names.stream().anyMatch(n -> n.length() > 3);
        // // equivalent in imperative style, procedural programming is this:
        // boolean nameWithMoreThan3LettersExists = false;
        // for (String name: names) {
        //     if(name.length() > 3){
        //         nameWithMoreThan3LettersExists = true;
        //         break;
        //     }
        // }
    }
}

class Person {
    String name;
    int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "{" + name + " : " + age + '}';
    }
}
