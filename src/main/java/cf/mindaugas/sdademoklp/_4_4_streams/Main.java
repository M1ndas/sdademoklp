package cf.mindaugas.sdademoklp._4_4_streams;

import org.w3c.dom.ls.LSOutput;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        // 0. Creating from collections
        List<String> names = Arrays.asList("Jimmy", "Jill", "Joe", "Anna");
        Stream<String> s = names.stream();
        s.forEach(System.out::println);

        //llegalStateException: stream has already been operated upon or closed
        // s.forEach(System.out::println);
        names.stream().forEach(System.out::println);

        // 1. FindAny() example
        Optional<String> firstName = names.stream().findAny();
        firstName.ifPresent(System.out::println);

        // ... Map, filter, reduce and so on.

        // 0. Simple example
        List<String> namesStartingWithA = names.stream()
                .filter(name -> name.startsWith("A"))
                .collect(Collectors.toList());
        System.out.println(namesStartingWithA);

        // 1. More complex example
        Optional<String> resultOfFilter = names
                .stream().parallel()
                .map(p -> p.toUpperCase())
                .filter(p -> p.charAt(0) == 'J')
                .map(x -> x.replace("O", "M"))
                .findAny();

        System.out.println("---------------------");
        resultOfFilter.ifPresent(System.out::println);

        System.out.println(names
                        .stream().parallel()
                        .map(p -> p.toUpperCase())
                        .filter(p -> p.charAt(0) == 'J')
                        .map(x -> x.replace("O", "M"))
                        .count());

        // Convert all string numbers into integers and return the even ones
        List<String> numbers = Arrays.asList("1", "2", "3", "4", "5", "6");
        System.out.println("original list: " + numbers);
        List<Integer> even = numbers.stream()
                .map(s1 -> Integer.valueOf(s1))
                .filter(number -> number % 2 == 0)
                .collect(Collectors.toList());

        System.out.println("processed list, only even numbers: " + even);
    }
}
