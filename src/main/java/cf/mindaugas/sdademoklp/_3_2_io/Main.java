package cf.mindaugas.sdademoklp._3_2_io;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // Creating a file object that will represent a file object in programs memory
        File absoluteFile = new File("C:/myDirectory/f.csv");
        File relativeFile = new File("files/myFile.txt");
        // escape sequences [\n, \r, \t, \s, ]
        File mavenFile = new File("target\\classes\\textFiles\\x.csv");

        // to know the current working directory
        System.out.println("Working Directory = " + System.getProperty("user.dir"));

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(mavenFile));

            // // 0.
            // String fileLine = bufferedReader.readLine();
            // while (fileLine != null) {
            //     System.out.println(fileLine);
            //     fileLine = bufferedReader.readLine();
            // }

            // 1.
            String fileLine;
            List<Person> people = new ArrayList<>();

            // 2. Simple version for reading file line by line
            System.out.println("------------- Simple version for reading file line by line ------------");
            while ((fileLine = bufferedReader.readLine()) != null) {
                System.out.println(fileLine);
            }
            System.out.println("----------------------------------------------------------------------");

            // 2. Some logic to handle missing values
            System.out.println("------------- Some logic to handle missing values ------------");
            BufferedReader bufferedReader2 = new BufferedReader(new FileReader(mavenFile));
            String id = "";
            String name = "";
            String age = "";
            while ((fileLine = bufferedReader2.readLine()) != null) {
                String[] stringParts = fileLine.split(",");
                for (int i = 0; i < stringParts.length; i++) {
                    if (fileLine.split(",")[i] != null) {
                        if (i == 0) {
                            id = fileLine.split(",")[i];
                        } else if (i == 1) {
                            name = fileLine.split(",")[i];
                        } else if (i == 2) {
                            age = fileLine.split(",")[i];
                        }
                    }
                }

                if (!id.isEmpty() && !age.isEmpty()) {
                    Person p = new Person(Integer.parseInt(id), name, Integer.parseInt(age));
                    people.add(p);
                }
                id = name = age = "";
            }

            System.out.println(people);

            int sum = 0;
            for (int i = 0; i < people.size(); i++)
                sum += people.get(i).age;

            System.out.println("Average age: " + sum / people.size());

            System.out.println("Let's sort the objects");
            System.out.println(people);
            Collections.sort(people);
            System.out.println(people);

            bufferedReader.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // writing (appending)
        try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(mavenFile,true))){
            String fileLine = "\nappended file line";
            bufferedWriter.write(fileLine);
        } catch(IOException e){
            e.printStackTrace();
        }
    } // main()
} // class

class Person implements Comparable<Person> {
    int id;
    String name;
    int age;

    public Person(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "{" + id + " : " + name + " : " + age + "}";
    }

    @Override
    public int compareTo(Person person) {
        return Integer.compare(this.age, person.age);
    }
}

